package org.springboottrial.topics;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class TopicController {
    @Autowired
    TopicService topicService;

    @RequestMapping("topics")
    public ArrayList<Topic> getAllTopics(){
        return topicService.getAllTopics();
    }

    @RequestMapping("topic/{id}")
    public Topic getATopic(@PathVariable int id) {
        return topicService.getATopic(id);
    }

    @PostMapping( "/topic")
    public boolean addTopic(@RequestBody Topic add) {
        return topicService.addTopic(add);
    }

    @DeleteMapping("/topic/{id}")
    public boolean deleteTopic(@PathVariable int id) {
        topicService.deleteTopic(id);
        return true;
    }

    @PutMapping("/topic/{id}")
    public boolean updateTopic(@RequestBody Topic topic) {
        return topicService.updateTopic(topic);
    }
}
