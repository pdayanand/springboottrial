package org.springboottrial.topics;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;

import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@Service
public class TopicService {

    private ArrayList<Topic> topics;

    public TopicService () {
        topics = new ArrayList<>( Arrays.asList(new Topic(1, "Topic 1", "JAVA"),
                new Topic(2, "Topic 2", ".NET"),
                new Topic(3, "Topic 3", "C++")));
    }

    public ArrayList<Topic> getAllTopics(){
        return topics;
    }

    public Topic getATopic( int id) {
        return topics.stream().filter(x -> x.getId() == id).findFirst().get();
    }

    public boolean addTopic( Topic add) {
        return topics.add(add);
    }


    public boolean deleteTopic( int id) {
        topics.remove(topics.stream().filter(x -> x.getId() == id).findFirst().get());
        return true;
    }

    public boolean updateTopic( Topic topic) {
        Topic toUpdate = topics.stream().filter(x -> x.getId() == topic.getId()).findFirst().get();
        toUpdate.setDesc(topic.getDesc());
        toUpdate.setName(topic.getName());
        return  true;
    }
}
